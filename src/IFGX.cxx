#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"
#include "ConfigSvc.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include "CutsBase.h"
#include "CutsIFGX.h"
#include "IFGX.h"

using namespace std;
using namespace Ort;

// Constructor
IFGX::IFGX(string modelFname)
  : m_onnx_svc(modelFname), Analysis()
{
  // Set up the expected event structure, include branches required for analysis.
  // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
  // Load in the Single Scatters Branch
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("pulsesTPC");
  m_event->Initialize();
  ////////

  // Setup logging
  logging::set_program_name("IFGX Analysis");
  
  // Setup the analysis specific cuts.
  m_cutsIFGX = new CutsIFGX(m_event);
  
  // Setup config service so it can be used to get config variables
  m_conf = ConfigSvc::Instance();
}

// Destructor
IFGX::~IFGX() {
  delete m_cutsIFGX;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
// 
// Initialize() -  Called once before the event loop.
void IFGX::Initialize() {
  INFO("Initializing IFGX Analysis");

  // Read in bPMT locations
  string alpacaTopDir = getenv("ALPACA_TOPDIR");
  m_BPMTLocations = ReadBPMTLocations(alpacaTopDir+"/modules/IFGX/BPMTLocations.csv");

  // Get the TTree ready
  m_tree = m_hists->BookTree("IFGX_Analysis", "event_id", &m_eventID, "l");
  m_tree->Branch("identity", &m_identity);
  m_tree->Branch("clusterSize", &m_clusterSize);
  m_tree->Branch("peakFracBot", &m_peakFracBot);
  m_tree->Branch("topBottomAsymmetry", &m_topBottomAsymmetry);
  m_tree->Branch("driftTime_us", &m_driftTime_us);
  m_tree->Branch("iForestScore", &m_scores);
}

// Execute() - Called once per event.
void IFGX::Execute() {
  // Prepare your input data here. You will need to get things from the
  // input trees and package them into the correct kind of tuple. A
  // suggestion is provided based on the model you supplied.

  int numSS = (*m_event->m_singleScatter)->nSingleScatters;

  if(numSS)
  {
    auto& tpcPulses = (*m_event->m_tpcPulses);
    const auto& chID = tpcPulses->chID;
    const auto& chPulseArea_phd = tpcPulses->chPulseArea_phd;
    const auto& bottomCentroidX_cm = tpcPulses->bottomCentroidX_cm;
    const auto& bottomCentroidY_cm = tpcPulses->bottomCentroidY_cm;
    const auto& s1Probability = tpcPulses->s1Probability;
    const auto& tba = tpcPulses->topBottomAsymmetry;
    m_driftTime_us = (*m_event->m_singleScatter)->driftTime_ns/1000.;
    int iS1 = 0;

    // Calculate RQs
    CreateDerivedRQs( s1Probability, chID, chPulseArea_phd, bottomCentroidX_cm, bottomCentroidY_cm,
                      iS1, m_clusterSize, m_peakFracBot );

    m_topBottomAsymmetry = tba[iS1];

    // Be sure that the iForest is trained with inputs in this order! 
    vector<float> float_input = {m_clusterSize, m_peakFracBot, m_topBottomAsymmetry, m_driftTime_us};
    auto input_tuple = make_tuple(float_input);

    // Run ML
    m_onnx_svc.ProcessInput(input_tuple, Preprocess);

    // Extract the outputs and fill tree
    vector<float> scores = m_onnx_svc.GetOutputDataFromNode<float>(1, Postprocess_scores);
    m_scores = 0.5 - scores.front(); // Subtract by baseline score, result will have normal (low score) and outlier (high score).

    m_tree->Fill();
  }
  m_eventID++;
}

// Implement some preprocessing of inputs here if you need to. See
// example modules for more information on how to do this.
void IFGX::Preprocess(ONNXModelSvc<float>::input_nodes_types& input_nodes)
{}

void IFGX::CreateDerivedRQs(
  const vector<float>&         s1Probability,
  const vector<vector<int>>&   chIDs,
  const vector<vector<float>>& chPulseArea_phd,
  const vector<float>&         bottomCentroidX_cm,
  const vector<float>&         bottomCentroidY_cm,
  int&                         iS1,
  float&                       clusterSize,
  float&                       peakFracBot)
{
  for(size_t i=0; i<bottomCentroidY_cm.size(); i++)
  {
    iS1 = i;
    if (s1Probability[i] > 0.5)    
    {
      vector<int> chIDRow;
      vector<float> chBottomAreaRow;
      float bSum = 0;

      for(size_t j=0; j<chIDs[i].size(); j++)
      {
        if(chIDs[i][j] >= 300 && chIDs[i][j] <= 540)
        {
          chIDRow.push_back(chIDs[i][j]);
          chBottomAreaRow.push_back(chPulseArea_phd[i][j]);
          bSum += chPulseArea_phd[i][j];
        }
      }
      if(chIDRow.size() == 0 || bSum == 0)
      {
        clusterSize = 70;
        peakFracBot = 0;
      }
      else
      {
        clusterSize = 0;
        for(size_t j=0; j<chIDRow.size(); j++)
        {
          float x = m_BPMTLocations[chIDRow[j]]["X(mm)"];
          float y = m_BPMTLocations[chIDRow[j]]["Y(mm)"];
          clusterSize += chBottomAreaRow[j] * sqrt(pow(x-bottomCentroidX_cm[i], 2) + pow(y-bottomCentroidY_cm[i], 2));
        }
        clusterSize /= bSum;
        peakFracBot = (*max_element(chBottomAreaRow.begin(), chBottomAreaRow.end())) / bSum;
      }
      return;
    }
  }
}

// Need to read in the bottom PMT locations from a CSV file: stolen from Micah
map<size_t, map<string, float>>
IFGX::ReadBPMTLocations(string fname)
{
  string line;
  ifstream csv_file(fname);
  map<size_t, map<string, float>> out_map;
  if(csv_file.is_open())
  {
    vector<string> headers;
    vector<float> data;

    //First read the headers
    getline(csv_file, line);
    headers = LineToVector<string>(line);
    headers.erase(headers.begin());
    while(getline(csv_file, line))
    {
      data = LineToVector<float>(line);
      size_t channel = data[0];
      data.erase(data.begin());
      map<string, float> tmp;
      for(size_t i=0; i<data.size(); i++)
        tmp[headers[i]] = data[i]/10.0; //Convert from mm to cm
      out_map[channel] = tmp;
    }
    csv_file.close();
  }
  else
  {
    // Warn if file not there/can't open
    stringstream ss;
    ss << "Couldn't open " << fname;
    WARNING(ss.str());
  }

  return out_map;
}

// Here's this utility method to convert a string to a vector of
// specified type, given a seperator. Stolen from Micah
template <typename T>
vector<T> IFGX::LineToVector(string line, string sep)
{
    vector< T > input_set;
    size_t pos = line.find(sep);
    T buf;
    while( true )
    {
        if constexpr(is_same<T, float>::value)
            buf = stof(line.substr(0, pos));
        else if constexpr(is_same<T, string>::value)
            buf = line.substr(0, pos);
        else if constexpr(is_same<T, double>::value)
            buf = stod(line.substr(0, pos));
        else if constexpr(is_same<T, int>::value)
            buf = stoi(line.substr(0, pos));
        else if constexpr(is_same<T, long>::value)
            buf = stol(line.substr(0, pos));
        else if constexpr(is_same<T, long double>::value)
            buf = stold(line.substr(0, pos));
        else if constexpr(is_same<T, long long>::value)
            buf = stoll(line.substr(0, pos));
        else if constexpr(is_same<T, unsigned long>::value)
            buf = stoul(line.substr(0, pos));
        else if constexpr(is_same<T, unsigned long long>::value)
            buf = stoull(line.substr(0, pos));
        else
        {
            cerr << "Can't process line of type " << typeid(T).name()
                 << ". Please choose float, double, string, int, long, long"
                 << " double, long long, unsigned long or unsigned long long."
                 << endl;
            return input_set;
        }
        input_set.push_back(buf);
        if( pos == string::npos ) break;
        line = line.substr(pos+1);
        pos = line.find(sep);
    }
    return input_set;
}

// Implement some postprocessing of outputs here if you need to.
void IFGX::Postprocess_scores(size_t i, vector<float>& scores)
{
}

// Finalize() - Called once after event loop.
void IFGX::Finalize() {
  INFO("Finalizing IFGX Analysis");
}

