#ifndef IFGX_H
#define IFGX_H

#include "Analysis.h"

#include "EventBase.h"
#include "CutsIFGX.h"
#include "ONNXModelSvc.h"

#include <TTreeReader.h>
#include <string>

class IFGX : public Analysis {

public:
  IFGX( std::string modelFname ); 
  ~IFGX();

  void Initialize();
  void Execute();
  void Finalize();

  static void Preprocess(ONNXModelSvc<float>::input_nodes_types& input_nodes);

  static void Postprocess_scores(size_t i, std::vector<float>& scores);

protected:
  ONNXModelSvc<float> m_onnx_svc;

  CutsIFGX* m_cutsIFGX;
  ConfigSvc* m_conf;

  // Output TTree and variables we will need
  TTree* m_tree;
  unsigned long m_eventID;
  float m_identity;
  float m_clusterSize;
  float m_peakFracBot;
  float m_topBottomAsymmetry;
  float m_driftTime_us;
  float m_scores;

  void CreateDerivedRQs( const std::vector<float>&              s1Probability,
                         const std::vector<std::vector<int>>&   chIDs,
                         const std::vector<std::vector<float>>& chPulseArea_phd,
                         const std::vector<float>&              bottomCentroidX_cm,
                         const std::vector<float>&              bottomCentroidY_cm,
                         int&                                   iS1,
                         float&                                 clusterSize,
                         float&                                 peakFracBot);

  // A method to read in a CSV file containing the locations of all the
  // bottom PMTs
  std::map<size_t, std::map<std::string, float>>
                    ReadBPMTLocations( std::string fname );

  template <typename T>
  std::vector<T> LineToVector(std::string line, std::string sep=",");

  std::map<size_t, std::map<std::string, float>> m_BPMTLocations;

};

#endif
