#ifndef CutsIFGX_H
#define CutsIFGX_H

#include "EventBase.h"

class CutsIFGX  {

public:
  CutsIFGX(EventBase* eventBase);
  ~CutsIFGX();
  bool IFGXCutsOK();


private:
  
  EventBase* m_event;

};

#endif
